package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/index.html", "templates/header.html", "templates/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	t.ExecuteTemplate(w, "index", nil)
	request := r.FormValue("request")
	if request == "" {
		request = "Empty request"
	}
	fmt.Fprintf(w, "<div class=\"col-sm-3\"></div><h3 class=\"col-sm-6\">your request is: %s</h3><div class=\"col-sm-3\"></div> ", request)
}

// ENTER point

func main() {
	fmt.Println("listening on port :5000")

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets/"))))

	http.HandleFunc("/", indexHandler)

	http.ListenAndServe(":5000", nil)
}
