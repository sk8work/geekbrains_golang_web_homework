package searcher

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
)

// Searcher function finding some string in html page and print source and true or false
func Searcher(VerifyString string, Sites []string) {
	for i := 0; i < len(Sites); i++ {
		resp, err := http.Get(Sites[i])

		if err != nil {
			fmt.Println(err)
			return
		}
		defer resp.Body.Close()

		htmlData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		isVerified, err := regexp.MatchString(VerifyString, string(htmlData))
		if err != nil {
			fmt.Println(err)
			return
		}

		if isVerified {
			fmt.Println(Sites[i], "\t", isVerified)
		} else {
			fmt.Println(Sites[i], "\t", isVerified)
		}
	}
}
