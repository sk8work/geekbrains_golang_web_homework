package main

import (
	"webdev/homework/1/crowl/searcher"
)

func main() {
	sites := make([]string, 0, 10)
	sites = append(sites, "https://yandex.com/")
	sites = append(sites, "https://google.com/")
	sites = append(sites, "https://golang.org/")
	sites = append(sites, "https://geekbrains.ru/")
	sites = append(sites, "https://gitlab.com/")

	searcher.Searcher("hello", sites)
}

// func searcher(VerifyString string, Sites []string) {
// 	for i := 0; i < len(Sites); i++ {
// 		resp, err := http.Get(Sites[i])

// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		defer resp.Body.Close()

// 		htmlData, err := ioutil.ReadAll(resp.Body)
// 		if err != nil {
// 			fmt.Println(err)
// 			// os.Exit(1)
// 		}

// 		isVerified, err := regexp.MatchString(VerifyString, string(htmlData))
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}

// 		if isVerified {
// 			fmt.Println(Sites[i], "\t", isVerified)
// 		} else {
// 			fmt.Println(Sites[i], "\t", isVerified)
// 		}
// 	}
// }
