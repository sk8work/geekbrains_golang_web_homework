package main

import (
	"log"
	"os"
	"os/signal"

	"github.com/sacOO7/gowebsocket"
)

func main() {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	socket := gowebsocket.New("ws://echo.websocket.org")
	socket.OnConnected = func(socket gowebsocket.Socket) {
		log.Println("Connection exist")
	}

	socket.OnConnectError = func(err error, socket gowebsocket.Socket) {
		log.Println("Connaction fail: ", err)
	}

	socket.OnPingReceived = func(data string, socket gowebsocket.Socket) {
		log.Println("Ping recieved " + data)
	}

	socket.Connect()

	for {
		select {
		case <-interrupt:
			log.Println("Connection closed")
			return
		}
	}
}
