package main

type PersonJSON struct {
	FirstName    string      `json:"firstName"`
	LastName     string      `json:"lastName"`
	Home         AddressJSON `json:"addres"`
	PhoneNumbers []string    `json:"phoneNumber"`
}

type AddressJSON struct {
	Street   string     `json:"streetAddress"`
	City     string     `json:"city"`
	PostCode PostalCode `json:"postalCode"`
}

type PostalCode int

type PersonXML struct {
	FirstName    string      `xml:"firstName"`
	LastName     string      `xml:"lastName"`
	Home         AddressJSON `xml:"addres"`
	PhoneNumbers []string    `xml:"phoneNumber"`
}

type AddressXML struct {
	Street   string     `xml:"streetAddress"`
	City     string     `xml:"city"`
	PostCode PostalCode `xml:"postalCode"`
}

// const (
// 	infoJSON = "info.json"
// 	infoXML = "info.xml"
// )

// func unmarshalJSON() error {
// 	body, err := openAndReadFile(infoJSON)
// 	if err != nil {
// 		return err
// 	}
// 	person := new(PersonJSON)
// 	if err := json.Unmarshal(body, person); err != nil {
// 		return err
// 	}
// 	log.Printf("LastName: %v; FirstName: %v", person.LastName, person.FirstName)
// }
