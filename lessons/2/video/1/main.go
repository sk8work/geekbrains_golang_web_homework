package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	if err := unmarshalJSON(); err != nil {
		log.Println(err)
	}
}

func marshallJSON() error {
	person := PersonJSON{
		FirstName: "Ivan",
		LastName:  "Ivanov",
		Home: AddressJSON{
			Street:   "Leninskie Gory",
			City:     "Moscow",
			PostCode: 111,
		},
		PhoneNumbers: nil,
	}

	object, err := json.Marshal(person)
	if err != nil {
		return err
	}

	log.Printf("%s", object)
	return nil
}

const (
	infoJSON = "info.json"
	infoXML  = "info.xml"
)

func unmarshalJSON() error {
	body, err := openAndReadFile(infoJSON)
	if err != nil {
		return err
	}
	person := new(PersonJSON)
	if err := json.Unmarshal(body, person); err != nil {
		return err
	}
	log.Printf("LastName: %v; FirstName: %v", person.LastName, person.FirstName)
	return nil
}

func openAndReadFile(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(file)
}
